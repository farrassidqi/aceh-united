<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'project_acehunited');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!5hKk=og*n/~i5!lY,6Gu^oS[Do{84Ol+a)G%tdbD?}q<X%&rcK-Y,NT7-dyy[lq');
define('SECURE_AUTH_KEY',  '%wI/LhhZvDVSOP8?fCG4xSm_D>+SWqA>zQo8|T<|<A.n~@*i{hKHs^?-9Pkb*xwo');
define('LOGGED_IN_KEY',    'C$WYJRPEAx==N,P]M83id`0jxY=I:9LU*0Z&,v6:*)6. MT[+~st<1j{5Os!&vu<');
define('NONCE_KEY',        ':lX%Y(kdU)tW*s`#G:0-#Zow-YyA1^T> z,Q%Jk[tGF2dJEmO@9::&&{!GwIh:G?');
define('AUTH_SALT',        '*dQF&3su_Gg.zbV-$,LlhAWN&^$dVB(gSwU<,ZGqM.w_m;?<$sw]]){/q$,|In6`');
define('SECURE_AUTH_SALT', ')_jL`_+fqffZKR56mhw/aQB;F +V{)sAZ`HKxC-vboOdrms,nU?NX6S3^-=k_h!F');
define('LOGGED_IN_SALT',   'vVSl,%]O9ipm3yH1UTuS0X&3TMGyR;s@=C[WAQ/?TWeC=oMg6JtPtZVQsW,G2p<C');
define('NONCE_SALT',       'r*%:b/ T*N?8?&14E&x@c6 yn;}#{vu/7+5Wtg.HRM(8q~_33^Eg/r:<fnh,8y+4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'm0l_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
// define('FS_METHOD','direct')
// define("FTP_HOST", "localhost");
// define("FTP_USER", "username");
// define("FTP_PASS", "ganteng");
